# Gitlabs Status

Ping Gitlab for 5 minutes and find the response time and report.

### Options

1. (not yet implemented) net/http package based.
2. (future plan) ping tool integration
3. (future plan) pingdon api integeration

```
./gitlab_status -s=(default|ping|pingdom) # in case of default omit -s flag)
```

## Coding standards

_I had been using pre-commit hook as my CI. It is simple._

- instant gratification, as it runs locally.
- because it runs before commit, no code with quality issue is pushed to repository.

Con:
- blocked for all the pipeline to complete, before commiting.

```
# To-Do create .setup file
take care of OS based pre-commit install
```

Using hooks provided by https://pre-commit.com/ to do code quality analysis.

After installing pre-commit as described in the above website.

run `pre-commit install` in the repo to install the pre-commit hook, which will check our code quality before commiting.

The pre commit hooks checks using go fmt, go vet, go lint, gometalinter.


ref: [https://github.com/dnephin/pre-commit-golang](https://github.com/dnephin/pre-commit-golang)

_When Gitlab test project demanded CI, it made me think a better hosted approach to my personal projects._

And I found [pantomath/go-tools-gitlab-how-to-do-continuous-integration-like-a-boss](https://medium.com/pantomath/go-tools-gitlab-how-to-do-continuous-integration-like-a-boss-941a3a9ad0b6) interesting.

So I will be trying that in this project, as I can try the Gitlab ecosystem along that.

- This helped me refresh on Make file.

