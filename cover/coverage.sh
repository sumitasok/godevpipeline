#!/bin/sh
# PKG_LIST=$(go list ./... | grep -v /vendor/)
for package in $(go list ./... | grep -v /vendor/); do
	echo $package
	echo "----"
    go test -covermode=count -coverprofile="cover/${package##*/}.cov" "$package" ;
done
tail -q -n +2 cover/*.cov >> cover/coverage.cov
go tool cover -func=cover/coverage.cov